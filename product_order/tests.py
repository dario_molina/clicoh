import json

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from mixer.backend.django import mixer
from rest_framework import status
from rest_framework.test import APIClient

from product_order.models import Product, Order, OrderDetail


class TestsProductOrder(TestCase):

    def setUp(self):
        user = mixer.blend(User, is_active=True)
        self.client = APIClient()
        self.client.force_authenticate(user=user)

    def test_get_product(self):
        """
        check the response ´get´ of the product list
        """
        product = mixer.blend(Product)
        response = self.client.get(
            path=reverse('product-list'),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0].get('name'), product.name)
        self.assertEqual(response.data[0].get('id'), product.id)
        self.assertEqual(response.data[0].get('price'), product.price)
        self.assertEqual(response.data[0].get('stock'), product.stock)

    def test_update_product(self):
        """
        Test to verify if product was updated
        """
        product_json = {
            "id": "asd123",
            "name": "celular",
            "price": 12000.0,
            "stock": 95
        }
        product = mixer.blend(Product)
        response = self.client.put(
            path=reverse('product-detail', kwargs={'pk': product.id}),
            data=json.dumps(product_json),
            content_type='application/json'
        )

        # asserts
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('id'), product_json.get('id'))
        self.assertEqual(response.data.get('name'), product_json.get('name'))
        self.assertEqual(response.data.get('price'), product_json.get('price'))
        self.assertEqual(response.data.get('stock'), product_json.get('stock'))

    def test_create_order(self):
        """
        verifies that an order is generated in the database
        """
        product = mixer.blend(Product, stock=100)
        order = {
            "id": 1234,
            "order_details": [
                {
                    "product": product.id,
                    "quantity": 5
                }
            ]
        }

        response = self.client.post(
            path=reverse('order-list'),
            data=json.dumps(order),
            content_type='application/json'
        )
        order_query = Order.objects.first()
        order_resp = response.data

        # asserts
        self.assertEqual(order_query.id, order_resp['id'])
        self.assertEqual(product.id, order_resp['order_details'][0]['product'])

    def test_create_order_and_verify_stock(self):
        """
        verifies that stock is updated when an order is created
        """
        stock = 100
        quantity_requested = 5
        product = mixer.blend(Product, stock=stock)
        order = {
            "id": 1234,
            "order_details": [
                {
                    "product": product.id,
                    "quantity": quantity_requested
                }
            ]
        }

        self.client.post(
            path=reverse('order-list'),
            data=json.dumps(order),
            content_type='application/json'
        )
        product_query = Product.objects.first()

        # asserts
        self.assertEqual(product_query.stock, stock - quantity_requested)

    def test_delete_order_and_recovered_stock(self):
        """
        verifies that the stock is recovered when deleting an order
        """
        quantity = 100
        product = mixer.blend(Product, stock=0)
        order = mixer.blend(Order)
        mixer.blend(OrderDetail, product=product, order=order, quantity=quantity)
        response = self.client.delete(
            path=reverse('order-detail', kwargs={'pk': order.id}),
            content_type='application/json'
        )
        product_stock = Product.objects.first().stock
        self.assertEqual(product_stock, quantity)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertListEqual(list(Order.objects.all()), [])

    def test_create_order_with_duplicated_product_bad_request(self):
        """
        Verify if product is duplicated in the order. This return bad requests
        """
        product = mixer.blend(Product, stock=100)
        order_json = {
            "id": 1,
            "order_details": [
                {
                    "product": product.id,
                    "quantity": 2
                },
                {
                    "product": product.id,
                    "quantity": 10
                }
            ]
        }
        response = self.client.post(
            path=reverse('order-list'),
            data=json.dumps(order_json),
            content_type='application/json'
        )
        # asserts
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

