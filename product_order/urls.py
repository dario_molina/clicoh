from clicoh.routers import DefaultRouter
from product_order.views import ProductViewSet, OrderViewSet, OrderDetailViewSet

router = DefaultRouter()

router.register('product', ProductViewSet, basename='product')
router.register('order', OrderViewSet, basename='order')
router.register('order_detail', OrderDetailViewSet, basename='order_detail')
