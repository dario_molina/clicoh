from django.db import models
from django.db.models import Sum, ExpressionWrapper, F, FloatField, Value
from django.db.models.functions import Coalesce


class Product(models.Model):
    id = models.CharField(primary_key=True, max_length=200)
    name = models.CharField(max_length=200)
    price = models.FloatField()
    stock = models.IntegerField()

    def __str__(self):
        return '{}-{}'.format(self.id, self.name)


class Order(models.Model):
    id = models.IntegerField(primary_key=True)
    date_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.id)

    @property
    def get_total(self):
        """
        Method to get the order total in pesos
        """
        total = self.query_total(dollar_price=1)
        return total

    def total_usd(self, dollar_price):
        """
        Method to get order total in usd
        """
        total = self.query_total(dollar_price=dollar_price)
        return total

    def query_total(self, dollar_price):
        query_total = self.order_details.select_related('product').annotate(
            total_product=Coalesce(ExpressionWrapper(
                F('product__price') * F('quantity') / dollar_price, output_field=FloatField()), Value(0))
        ).aggregate(
            total=Sum('total_product')
        )
        total = round(query_total.get('total'), 2) or float('0')
        return total


class OrderDetail(models.Model):
    order = models.ForeignKey('product_order.Order', on_delete=models.CASCADE, related_name='order_details')
    quantity = models.IntegerField()
    product = models.ForeignKey('product_order.Product', on_delete=models.CASCADE, related_name='product_order_details')

    def __str__(self):
        return '{}-{}'.format(self.order.id, self.product.name)
