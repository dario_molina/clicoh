import requests
from django.db import transaction
from rest_framework import serializers
from rest_framework.fields import empty

from clicoh.constants import dollar_url
from product_order.models import Product, Order, OrderDetail


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class OrderDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = '__all__'

    @transaction.atomic
    def create(self, validated_data):
        product = validated_data.get('product')
        order = validated_data.get('order')
        exists_product = order.order_details.filter(product=product).exists()
        if exists_product:
            raise serializers.ValidationError(
                "This product ({0} - {1}) already"
                " exists in the order {2}".format(product.id, product.name, order.id)
            )
        quantity = validated_data.get('quantity')
        product.stock -= quantity
        product.save()
        return super(OrderDetailSerializer, self).create(validated_data)

    @transaction.atomic
    def update(self, instance, validated_data):
        quantity = validated_data.get('quantity')
        product = validated_data.get('product')
        quantity_diff = quantity - self.instance.quantity
        new_stock = product.stock - quantity_diff
        Product.objects.filter(id=product.id).update(stock=new_stock)
        return super(OrderDetailSerializer, self).update(instance, validated_data)

    def validate(self, attr):
        request = self.context['request']
        quantity = attr.get('quantity')
        product = attr.get('product')
        if request.stream.META['REQUEST_METHOD'] == 'POST' and quantity > product.stock:
            raise serializers.ValidationError(
                "The requested quantity is greater than the available stock."
                "[stock available: {0} - quantity requested: {1}]".format(product.stock, quantity)
            )
        if request.stream.META['REQUEST_METHOD'] in ['PUT', 'PATCH']:
            quantity_diff = quantity - self.instance.quantity
            if quantity_diff > product.stock:
                raise serializers.ValidationError(
                    "The requested quantity is greater than the available stock."
                    "[stock available: {0} - quantity requested: {1}]".format(product.stock, quantity_diff)
                )
        return attr

    @staticmethod
    def validate_quantity(quantity):
        if quantity <= 0:
            raise serializers.ValidationError("Quantity must be greater than zero")
        return quantity


class OrderProductDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = [
            'product',
            'quantity'
        ]

    @staticmethod
    def validate_quantity(quantity):
        if quantity <= 0:
            raise serializers.ValidationError("Quantity must be greater than zero")
        return quantity


class OrderSerializer(serializers.ModelSerializer):
    order_details = OrderProductDetailSerializer(many=True)
    total_usd = serializers.SerializerMethodField()

    def __init__(self, instance=None, data=empty, **kwargs):
        self.dollar_blue = self.get_dollar_price()
        super(OrderSerializer, self).__init__(instance=instance, data=data, **kwargs)

    class Meta:
        model = Order
        fields = [
            'id',
            'date_time',
            'get_total',
            'total_usd',
            'order_details',
        ]

    @transaction.atomic
    def create(self, validated_data):
        order_id = validated_data.get('id')
        order = Order.objects.create(id=order_id)
        order_details = validated_data.get('order_details')
        for order_detail in order_details:
            detail = OrderDetail.objects.create(order=order, **order_detail)
            product = detail.product
            product.stock -= detail.quantity
            product.save()
        return validated_data

    def get_total_usd(self, instance):
        dollar_blue = float(self.dollar_blue.replace(',', '.'))
        if isinstance(instance, Order):
            return instance.total_usd(dollar_blue)
        if isinstance(instance, dict):
            order_details = instance.get('order_details')
            total = 0
            for detail in order_details:
                product_price = detail.get('product').price
                total += detail.get('quantity') * product_price
            total_usd = float(total) / float(dollar_blue)
            return round(total_usd, 2)

    def validate(self, validated_data):
        order_details = validated_data.get('order_details')
        product_list = [detail.get('product') for detail in order_details]
        if len(product_list) != len(set(product_list)):
            raise serializers.ValidationError(
                {"order_details": "There are more than 1 repeated product in the order details."})
        for detail in order_details:
            quantity_in_stock = detail.get('product').stock - detail.get('quantity')
            if quantity_in_stock < 0:
                message = "The product {0} is out of stock. [stock available: {1} - quantity requested: {2}]"
                message = message.format(
                    detail.get('product'),
                    detail.get('product').stock,
                    detail.get('quantity'))
                raise serializers.ValidationError({"order_details": message})
        return validated_data

    @staticmethod
    def get_dollar_price():
        """
        Method to obtain dollar price
        """
        response = requests.get(url=dollar_url)
        response_data = response.json()
        dollar_data = list(filter(lambda val: val['casa']['nombre'] == 'Dolar Blue', response_data))[0]

        dollar_blue = dollar_data['casa']['venta']
        return dollar_blue
