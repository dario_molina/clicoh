from django.db import transaction
from rest_framework import viewsets, status, mixins
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from product_order.models import Product, Order, OrderDetail
from product_order.serializers import ProductSerializer, OrderSerializer, OrderDetailSerializer


class ProductViewSet(viewsets.ModelViewSet):
    """
    Product view set
    """
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


class OrderViewSet(mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.DestroyModelMixin,
                   mixins.ListModelMixin,
                   GenericViewSet):
    """
    Order view set
    """
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    @transaction.atomic
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        for order_detail in instance.order_details.all():
            stock_recovery = order_detail.product.stock + order_detail.quantity
            Product.objects.filter(id=order_detail.product.id).update(stock=stock_recovery)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class OrderDetailViewSet(viewsets.ModelViewSet):
    """
    Order Detail view set
    """
    serializer_class = OrderDetailSerializer
    queryset = OrderDetail.objects.all()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.product.stock += instance.quantity
        instance.product.save()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
