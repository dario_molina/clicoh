# README # [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

#### Instructivo de uso de api

##### Autenticacion
Previamente se generó un usuario con las siguientes credenciales
        
        {"username": "clicoh", "password": "clicoh1234"}

El sistema de authenticacion es mediante JWT.
Podemos generar el token para un usuario registrado en la base de datos de la siguiente manera

``` curl
curl \
  -X POST \
  -H "Content-Type: application/json" \
  -d '{"username": "clicoh", "password": "clicoh1234"}' \
  https://blooming-bastion-98205.herokuapp.com/api/token/authentication/
```

La respuesta obtenida es:
``` json
{
    "refresh":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYwNjEwODU1OSwianRpIjoiMTkwZDI0ZWQ5ZjUzNGUzOGE0MDllY2U2Yjc1MDUyNWIiLCJ1c2VyX2lkIjoxfQ.Er4qx76aB-fr_J0bHJ-YKGH7W73CGcSuUHg7cQo1ApM",
    "access":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjA2MDI5MzU5LCJqdGkiOiIwNjg2NDRhZDE3Zjk0OWQ0YTEzNmRiMTgyYzU2MTE3NCIsInVzZXJfaWQiOjF9.YB_M19tsTY8A4yBY4GzeLIdixn458qPl8klqeUJl-hk"
}
```
En donde el campo `access` lo usaremos para autenticarnos y poder obtener respuesta de los endpoints. Si no adjuntamos el `access` no podremos obtener respuesta de los endpoints.

Para todos los siguientes endpoints usaremos el campo `access` que se obtuvo en el endpoint de autenticacion y lo reemplazaremos en el campo `<access>` de los siguientes ejemplos con curl.

##### Endpoints de Producto
* Listado de productos.

``` curl
curl -X GET -H "Content-Type: application/json" -d '{}' \
  -H "Authorization: Bearer <access>" \
  https://blooming-bastion-98205.herokuapp.com/product/
```

* Creacion de un producto

``` curl
curl -X POST -H "Content-Type: application/json" -d '{"id":"cel1234","name":"celular","price":12000,"stock":50}' \
  -H "Authorization: Bearer <access>" \
  https://blooming-bastion-98205.herokuapp.com/product/
```

* Detalle de un producto

En la url debemos agregar el id del producto, en este caso `cel1234`
``` curl
curl -X GET -H "Content-Type: application/json" -d '{}' \
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/product/cel1234/
```

* Actualizar un producto

En la url debemos agregar el id del producto, en este caso `cel1234`

``` curl
curl -X PUT -H "Content-Type: application/json" -d '{"id":"cel1234","name":"celular","price":15000,"stock":100}' \
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/product/cel1234/
```

* Eliminar un producto

En la url debemos agregar el id del producto, en este caso `cel1234`

``` curl
curl -X DELETE -H "Content-Type: application/json" -d '{}' \                                                   
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/product/cel1234/
```

##### Endpoints de Orden
* Lista de ordenes

``` curl
curl -X GET -H "Content-Type: application/json" -d '{}' \
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/order/
```

* Creacion de ordenes

En la creacion de una orden, tambien podemos generar varios order_details, agregando el id del producto y la cantidad deseada (internamente se valida que el producto exista y que haya stock disponible) como se muestra en el siguiente ejemplo de curl

``` curl
curl -X POST -H "Content-Type: application/json" -d '{"id": 123, "order_details": [{"product": "cel1234","quantity": 20}]}' \
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/order/
```

* Eliminar una orden

Al eliminar una orden, se eliminan todos los detalles de las ordenes también y el stock de los productos será recuperado

``` curl
curl -X DELETE -H "Content-Type: application/json" -d '{}' \
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/order/<order_id>/
```

##### Endpoint de Detalle de Orden
* Lista de todos los detalles de ordenes

``` curl
curl -X GET -H "Content-Type: application/json" -d '{}' \
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/order_detail/
```

* Creacion de un detalle de orden

Para generar un detalle de orden debemos tener creado previamente un producto y una orden. En los campos le pasamos la cantidad solicitada (`quantity`), el id de la orden (`order`) y el id del producto (`product`)

``` curl
curl -X POST -H "Content-Type: application/json" -d '{"quantity": 20, "order": 1, "product": "cel1234"}' \
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/order_detail/
```

* Update de un detalle de orden

Para actualizar un detalle de orden conocer su id y agregarle como parametro en la url. En los campos a actualizar le pasamos la cantidad solicitada (`quantity`), el id de la orden (`order`) y el id del producto (`product`) para ser actualizados. Tambien se puede usar PATCH.

``` curl
curl -X PUT -H "Content-Type: application/json" -d '{"quantity": 20, "order": 1, "product": "cel1234"}' \
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/order_detail/<order_detail_id>/
```

* Consulta del Detalle de una orden

Para obtener el detalle de la orden, debemos pasarle su id en la url, como se muestra en el ejemplo de curl 

``` curl
curl -X GET -H "Content-Type: application/json" -d '{}' \
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/order_detail/<order_detail_id>/
```

* Eliminar Detalle de una orden

Para eliminar el detalle de la orden, debemos pasarle su id en la url, como se muestra en el ejemplo de curl. Eliminar un detalle de orden, recupera el stock del producto en cuestion.

``` curl
curl -X DELETE -H "Content-Type: application/json" -d '{}' \
  -H "Authorization: Bearer <access>" \
  http://blooming-bastion-98205.herokuapp.com/order_detail/<order_detail_id>/
```